# -*- coding: UTF-8 -*-
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import f_regression
from sklearn.feature_selection import SelectKBest
from sklearn.cross_validation import cross_val_score, ShuffleSplit
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error, explained_variance_score
import matplotlib.pyplot as plt
from pprint import pprint
import numpy as np
import pandas as pd
from pandas import Series, DataFrame

class MachineLearningExam:
    """
    机器学习测验回归分类
    """
    # 异常点数量
    OUTLIER_NUM = 4

    def learn(self):
        """
        开始机器学习
        :returns: 无
        """
        # 数据读取
        self._read_data()
        # 分离特征值和目标值
        self._separate_feature_target()
        # 数据清洗
        self._clean_data()
        # 数据归一化
        self._normalize_data()
        # 特征选择
        self._select_feature()
        # 模型训练
        self._train_model()
        # 模型评估
        self._score_model()
    
    def _read_data(self):
        """
        从csv文件中读取数据
        :returns: 无
        """
        # 读取csv数据文件，分隔符默认为逗号，第一行为列数据标签
        self.data = pd.read_csv("ML Final Exam dataset.csv", header=0)
        # print("csv文件中数据集:", self.data.shape)

    def _separate_feature_target(self):
        """
        分离目标值和特征值
        :returns: 无
        """
        # 提取DevFDBK数据列作为目标值
        self.data_target = self.data["DevFDBK"]
        # 剩余列数据作为特征值
        self.data_feature = self.data.drop(["DevFDBK"], axis=1)
        # 原始特征值名称序列
        self.original_data_feature_names = self.data_feature.columns
    
    def _classify_feature(self, dev_fdbk_values):
        """
        目标值分类
        :param dev_fdbk_values: 目标值DevFDBK的数组
        :returns: 目标值分类数组
                0 - 没有缺陷的数据（non-defect）
                1 - 有缺陷的数据（defect）
        """
        data_type_target = []
        for value in dev_fdbk_values:
            if value <= 11:
                data_type_target.append(0)
            else:
                data_type_target.append(1)
        return data_type_target

    def _clean_data(self):
        """
        清洗数据
        :returns: 无
        """
        # 画出目标值箱型图
        plt.boxplot(x=self.data_target.values, labels=["DevFDBK"], sym =".")
        #plt.show()
        # 删除异常值数据
        for i in range(MachineLearningExam.OUTLIER_NUM):
            # 目标值中最大值所在行索引
            idxmax = self.data_target.idxmax()
            # 删除目标值中异常值数据
            self.data_target.drop(idxmax, inplace=True)
            # 删除特征值中异常值数据
            self.data_feature.drop(idxmax, inplace=True)
    
    def _normalize_data(self):
        """
        数据归一化
        :returns: 无
        """
        # 采用0均值标准化算法（Z-score）标准化特征值
        scaler = preprocessing.StandardScaler()
        scaler.fit(self.data_feature.values)
        #print(scaler.mean_)
        #print(scaler.std_)
        #测试将该scaler用于输入数据，变换之后得到的结果同上
        normalize_data = scaler.transform(self.data_feature.values)
        #print(normalize_data)
        feature_columns = self.data_feature.columns
        self.data_feature = DataFrame(normalize_data)
        self.data_feature.columns = feature_columns

    def _select_feature(self):
        """
        选择特征值
        :returns: 无
        """
        # 删除RangeFDBK特征值，它不影响目标值DevFDBK
        self.data_feature.drop(["RangeFDBK"], axis=1, inplace=True)
        # 删除取值变化小的特征值
        self._remove_feature_low_variance()

    def _remove_feature_low_variance(self):
        """
        删除取值变化小的特征值
        :returns: 无
        """
        # 使用VarianceThreshold算法，特征值相同率超过0.8，将被删除
        vt = VarianceThreshold(threshold=(.8 * (1 - .8)))
        # 拟合并转化数据
        vt.fit_transform(self.data_feature.values)
        #print(vt.variances_)   # 特征值的方差
        # 保留的特征数据列索引
        remain_indices = vt.get_support(indices=True)
        # 特征数据列名称
        feature_name = self.data_feature.columns.values
        # 要删除的特征数据列名称
        remove_feature_name = [feature_name[idx] 
                        for idx, _ in enumerate(feature_name)
                        if idx not in remain_indices]
        #print(remove_feature_name)
        # 删除取值变化小的特征数据列
        for name in remove_feature_name:
            self.data_feature.drop([name], axis=1, inplace=True)
        # print("删除取值变化小的特征值后特征值集: ", self.data_feature.shape)

    def _train_model(self):
        """
        训练模型
        :returns: 无
        """
        # 将数据切分成两部分，75%作为训练数据，25%作为测试数据
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(self.data_feature.values, self.data_target.values, random_state=1, test_size=0.25)
        # print("训练数据集:", self.x_train.shape, self.y_train.shape)
        # print("测试数据集:", self.x_test.shape, self.y_test.shape)

        # 参数调优
        #self._optimize_model_parameter()

        # 使用随机森林模型进行学习
        self._train_model_random_forest_regressor()
    
    def _optimize_model_parameter(self):
        """
        优化模型参数
        :returns: 无
        """
        # 将所有数据作为优化处理的训练数据
        x_train, x_test, y_train, y_test = train_test_split(self.data_feature.values, self.data_target.values, random_state=1, test_size=0.0)

        # 优化n_estimators参数
        param_test = {
            # 'n_estimators': range(10, 250, 10),
            'n_estimators': range(80, 100, 2)
        }
        model = RandomForestRegressor(max_leaf_nodes=10, max_depth=10, max_features='sqrt', oob_score=True, random_state=1)
        gsearch = GridSearchCV(estimator=model, param_grid=param_test, scoring='r2')
        gsearch.fit(x_train, y_train)
        pprint(gsearch.grid_scores_)
        print("n_estimators最高得分: %0.3f" % gsearch.best_score_)
        print("n_estimators最优参数:", gsearch.best_params_)

        # 优化max_leaf_nodes和max_depth参数
        param_test = {
            # 'max_leaf_nodes': range(5, 40, 5), 
            'max_leaf_nodes': range(28, 40, 2), 
            # 'max_depth': range(5, 50, 5), 
            'max_depth': range(6, 20, 2), 
        }
        model = RandomForestRegressor(n_estimators=90, max_features='sqrt', oob_score=True, random_state=1)
        gsearch = GridSearchCV(estimator=model, param_grid=param_test, scoring='r2')
        gsearch.fit(x_train, y_train)
        pprint(gsearch.grid_scores_)
        print("max_leaf_nodes和max_depth最高得分: %0.3f" % gsearch.best_score_)
        print("max_leaf_nodes和max_depth最优参数:", gsearch.best_params_)

        # 优化max_features参数
        param_test = {
            # 'max_features': range(5, 100, 5), 
            'max_features': range(6, 20, 2), 
        }
        model = RandomForestRegressor(n_estimators=90, max_leaf_nodes=30, max_depth=14, oob_score=True, random_state=1)
        gsearch = GridSearchCV(estimator=model, param_grid=param_test, scoring='r2')
        gsearch.fit(x_train, y_train)
        pprint(gsearch.grid_scores_)
        print("max_features最高得分: %0.3f" % gsearch.best_score_)
        print("max_features最优参数:", gsearch.best_params_)

    def _train_model_random_forest_regressor(self):
        """
        使用随机森林模型
        :returns: 无
        """
        # 创建岭回归模型
        #self.model = Ridge(alpha=1.0, fit_intercept=True, normalize=False, copy_X=True, max_iter=20, tol=0.001, solver='auto')
        # 创建随机森林模型
        self.model = RandomForestRegressor(n_estimators=90, oob_score=True, max_features=13, max_leaf_nodes=30, max_depth=14, random_state=1)
        # 拟合数据
        self.model.fit(self.data_feature.values, self.data_target.values)
        # 预测数据
        self.predict_target = self.model.predict(self.x_test)

    def _score_model(self):
        """
        根据测试数据结果评估模型
        :returns: 无
        """
        # 特征值相关系数
        coefficient = Series(self.model.feature_importances_, index = self.data_feature.columns)
        coefficient = coefficient.sort_values(ascending=False)
        print("特征值相关系数:")
        pprint(coefficient)
        # 评价模型
        print("拟合优度（R2决定系数）值为：", r2_score(self.y_test, self.predict_target))
        print("均方误差（MSE）值为：", mean_squared_error(self.y_test, self.predict_target))
        print("平均绝对值误差（MAE）值为：", mean_absolute_error(self.y_test, self.predict_target))
        print("可释方差得分值为：", explained_variance_score(self.y_test, self.predict_target))

if __name__ == "__main__":
    mlexam = MachineLearningExam()
    mlexam.learn()
